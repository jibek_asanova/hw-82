const express = require('express');
const User = require("../models/User");
const Track = require("../models/Track");
const TrackHistory = require("../models/TrackHistory");

const router = express.Router();

router.post('/', async (req, res) => {
  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  if (!req.body.track) {
    return res.status(400).send('Data not valid');
  }
  try {
    const track = await Track.findById(req.body.track);

    if (!track) {
      return res.status(404).send({error: 'Track not found'});
    }
    const trackHistoryData = {
      user: user._id,
      track: track,
      datetime: new Date(),
    };

    const trackHistory = new TrackHistory(trackHistoryData);

    await trackHistory.save();
    res.send(trackHistory);

  } catch (e) {
    res.sendStatus(500).send(e);
  }
});

module.exports = router;