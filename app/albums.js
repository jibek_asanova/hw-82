const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if(req.query.artist) {
      query.artist = req.query.artist;
    }
    const albums = await Album.find(query).populate('artist', 'title description');
    res.send(albums);
  } catch (e) {
    res.sendStatus(500)
  }
});

router.get('/:id', async (req, res) => {
  try {
    const album = await Album.findById(req.params.id).populate('artist', 'title description');
    if (album) {
      res.send(album);
    } else {
      res.sendStatus(404).send({error: 'Album not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('image'), async (req, res) => {
  if(!req.body.title || !req.body.artist) {
    return res.status(400).send('Data not valid');
  }

  const albumData = {
    title: req.body.title,
    artist: req.body.artist,
    releasedYear: req.body.releasedYear || null
  };

  if (req.file) {
    albumData.image = req.file.filename
  }

  const album = new Album (albumData);
  try {
    await album.save();
    res.send(album);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
